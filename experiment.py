import os
import random
from collections import deque
import json

import gym
import gym_super_mario_bros.actions as actions
from nes_py.wrappers import JoypadSpace
from gym import spaces
import numpy as np
import cv2
import keras
from keras.models import Sequential, clone_model
from keras.layers import Conv2D, Dense, Flatten
from keras.optimizers import Adam


from collections import deque
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import Session
from tensorflow.compat.v1.keras import backend as K
from tensorflow.compat.v1 import InteractiveSession

from numpy.random import seed
seed(2302845)
from tensorflow.random import set_seed
set_seed(2302845)

from wrappers import wrap_nes


"""

Majority taken from https://github.com/trustycoder83/super-mario-bros-v0

"""


class Agent:
    def __init__(self, env, memory_size=20000):
        self.env = env
        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n
        self.observation_shape = env.observation_space.shape
        self.memory = ReplyBuffer(memory_size=memory_size)
        self.batch_size = 32
        self.update_frequency = 4
        self.tau = 1000
        self.gamma = 0.99  # discount rate
        self.epsilon = 1  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.9999
        self.learning_rate = 0.0001
        self.model = self._build_model()
        self.target_model = self._build_model()
        self.target_model.set_weights(self.model.get_weights())

    def _build_model(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), strides=(4, 4), activation='relu', input_shape=self.observation_shape))
        model.add(Conv2D(64, (4, 4), strides=(2, 2), activation='relu'))
        model.add(Conv2D(64, (3, 3), strides=(1, 1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(512, activation='elu', kernel_initializer='random_uniform'))
        model.add(Dense(self.action_size, activation='softmax'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        return model

    def update_target_network(self):
        self.target_model = clone_model(self.model)
        self.target_model.set_weights(self.model.get_weights())

    def memorize(self, state, action, reward, next_state, done):
        self.memory.append(state, action, reward, next_state, done)

    def act(self, state):
        if random.uniform(0, 1) < self.epsilon:
            return random.randrange(self.action_size)
        else:
            return np.argmax(self.model.predict(state)[0])

    def experience_reply(self):
        if self.batch_size > len(self.memory):
            return

        # Get indices of samples for replay buffers
        indices = np.random.choice(range(len(self.memory)), size=self.batch_size)

        # Randomly sample a batch from the memory
        state_sample = np.array([self.memory.state[i][0] for i in indices])
        action_sample = np.array([self.memory.action[i] for i in indices])
        reward_sample = np.array([self.memory.reward[i] for i in indices])
        next_state_sample = np.array([self.memory.next_state[i][0] for i in indices])
        done_sample = np.array([self.memory.done[i] for i in indices])

        # Batch prediction to save speed
        target = self.model.predict(state_sample)
        target_next = self.target_model(next_state_sample)

        for i in range(self.batch_size):
            if done_sample[i]:
                target[i][action_sample[i]] = reward_sample[i]
            else:
                target[i][action_sample[i]] = reward_sample[i] + self.gamma * (np.amax(target_next[i]))

        self.model.fit(
            np.array(state_sample),
            np.array(target),
            batch_size=self.batch_size,
            verbose=0
        )

    def load_weights(self, weights_file):
        self.model.load_weights(weights_file)

    def save_weights(self, weights_file):
        self.model.save_weights(weights_file)


# Defines training related constants
num_episodes = 50000
max_wait = 100

# Initializes the environment
env = wrap_nes("SuperMarioBros-1-1-v0", actions.SIMPLE_MOVEMENT)


# --------------- Original --------------- #

init = {'episode': 0,
        'frame_count': 0,
        'max_reward': 0,
        'epsilon': 1}


# Creates an agent
agent = Agent(env=env, memory_size=20000)

for episode in range(init['episode'], num_episodes):

    total_reward = 0  # Defines the total reward per episode
    observation = env.reset()  # Resets the environment
    state = np.reshape(observation, (1,) + env.observation_space.shape)  # Gets the state

    for episode_step in range(env.spec.max_episode_steps):
        action = agent.act(state)  # Gets a new action

        # Takes action and calculates the total reward
        observation, reward, done, info = env.step(action)

        total_reward += reward

        next_state = np.reshape(observation, (1,) + env.observation_space.shape)  # Gets the next state

        agent.memorize(state, action, reward, next_state, done)  # Memorizes the experience

        # Updates the online network weights
        if init['frame_count'] % agent.update_frequency == 0:
            agent.experience_reply()

        # Updates the target network weights
        if init['frame_count'] % agent.tau == 0:
            agent.update_target_network()

        state = next_state  # Updates the state
        init['frame_count'] += 1  # Updates the total steps

        if done:
            break

    # Updates the epsilon value
    agent.epsilon = max(agent.epsilon_min, agent.epsilon * agent.epsilon_decay)
    ongoing_reward[episode] = total_reward
    ongoing_episode_length[episode] = episode_step

    # Saves the online network weights
    if episode % 10 == 0:
        agent.save_weights("super_mario_bros_1.h5")
        np.save('ongoing_reward_1.npy', ongoing_reward)
        np.save('ongoing_episode_length_1.npy', ongoing_episode_length)
        with open('init_1.json', 'w') as file:
            json.dump(init, file)

        keras.backend.clear_session()

# --------------- Experiment --------------- #

init = {'episode': 0,
        'frame_count': 0,
        'max_reward': 0,
        'epsilon': 1}

for episode in range(init['episode'], num_episodes):

    total_reward = 0  # Defines the total reward per episode
    max_x_pos = (0, 0)

    observation = env.reset()  # Resets the environment
    state = np.reshape(observation, (1,) + env.observation_space.shape)  # Gets the state

    for episode_step in range(env.spec.max_episode_steps):
        action = agent.act(state)  # Gets a new action

        # Takes action and calculates the total reward
        observation, reward, done, info = env.step(action)

        total_reward += reward

        """ If agent has not progressed forward in the last 100 steps when kill the agent."""
        if episode_step - max_x_pos[1] >= max_wait:
            total_reward -= 50.0 / 10.0
            done = True

        next_state = np.reshape(observation, (1,) + env.observation_space.shape)  # Gets the next state

        agent.memorize(state, action, reward, next_state, done)  # Memorizes the experience

        # Updates the online network weights
        if init['frame_count'] % agent.update_frequency == 0:
            agent.experience_reply()

        # Updates the target network weights
        if init['frame_count'] % agent.tau == 0:
            agent.update_target_network()

        state = next_state  # Updates the state
        init['frame_count'] += 1  # Updates the total steps

        """ Remember the highest x position and when it was reached"""
        if info['x_pos'] > max_x_pos[0]:
            max_x_pos = (info['x_pos'], episode_step)

        if done:
            break

    # Updates the epsilon value
    agent.epsilon = max(agent.epsilon_min, agent.epsilon * agent.epsilon_decay)
    ongoing_reward[episode] = total_reward
    ongoing_episode_length[episode] = episode_step

    init['episode'] = episode
    init['epsilon'] = agent.epsilon
    init['max_reward'] = max(init['max_reward'], total_reward)

    # Saves the online network weights
    if episode % 10 == 0:
        agent.save_weights("super_mario_bros_2.h5")
        np.save('ongoing_reward_2.npy', ongoing_reward)
        np.save('ongoing_episode_length_2.npy', ongoing_episode_length)
        with open('init_2.json', 'w') as file:
            json.dump(init, file)

        keras.backend.clear_session()

# Closes the environment
env.close()