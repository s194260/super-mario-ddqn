The wrappers.py file is directly from https://github.com/trustycoder83/super-mario-bros-v0.

The experiment.py file runs the original and the experimental algorithm and is primarily from https://github.com/trustycoder83/super-mario-bros-v0, including many of the comments.
However the code from https://github.com/trustycoder83/super-mario-bros-v0 needed some changes in order to work with our setup, newer libraries and bug fixes.
Furthermore, the code needed to be adapted in order to run on a HPC server for a long time and saving the performance during training.

The primarity changes created the better agent are:


```python
max_x_pos = (0, 0)
```
on line 196,

```python
if episode_step - max_x_pos[1] >= max_wait:
   total_reward -= 50.0 / 10.0
   done = True
```
on line 210-211 and

```python
if info['x_pos'] > max_x_pos[0]:
    max_x_pos = (info['x_pos'], episode_step)
```
on line 230-231.